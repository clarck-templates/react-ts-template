import React from "react";
import {HashRouter as Router, Routes, Route} from "react-router-dom"
import { ContactPage } from "../Pages/Contact/ContactPage";
import { HomePage } from "../Pages/Home/HomePage";



export const AppRouter = (props:any)=>{
    return (
        <Router>
            {props.children}
        </Router>
    )
}

export const AppRoutes = (props:any)=>{
    return (
        <Routes>
            <Route path="/" element={<HomePage/>} />
            <Route path="/contact" element={<ContactPage/>} />
        </Routes>
    )
}
