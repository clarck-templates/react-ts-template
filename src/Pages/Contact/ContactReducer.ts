import { createSlice } from "@reduxjs/toolkit";


export const contactSlice = createSlice({
    name: "contact",
    initialState: {
        message: "",
    },
    reducers:{
        submit: (state, action)=>{
            state.message = action.payload
        },
        clear: (state)=>{
            state.message = ""
        },
    }
})


export const { submit, clear } = contactSlice.actions
export default contactSlice.reducer