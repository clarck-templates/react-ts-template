import React from 'react';
import { AppRouter, AppRoutes } from './Routing/Routes';

function App() {
  return (
    <AppRouter>
      <div className="App">
        <AppRoutes />
      </div>
    </AppRouter> 
    
  );
}

export default App;
